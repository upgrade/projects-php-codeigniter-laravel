<?php include "crud/list_update.php"; ?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="js/jquery-3.3.1.js"></script>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    


    <title>Crud with php + PDO</title>
</head>
<body>
    
    <div class="container">
        <h3 class="text-center">Update</h3>

        <form action="crud/update.php" method="post">

            <label for="name_user">Name</label>
            <input type="text" name="name_user" id="name_user" class="form-control" value="<?php echo $return['name_user'] ?>" ><br>  

            
            <label for="email">E-Mail</label>
            <input type="text" name="email" id="email" class="form-control"  value="<?php echo $return['email'] ?>"><br>
        
            
            <label for="address">Address</label>
            <input type="text" name="address" id="address" class="form-control"  value="<?php echo $return['address'] ?>"><br>
            <input type="hidden" name="id" id="id" class="form-control"  value="<?php echo $return['id'] ?>"><br>

            <button type="submit" class="btn btn-success">Register</button>

        </form>

    </div>


</body>
</html>