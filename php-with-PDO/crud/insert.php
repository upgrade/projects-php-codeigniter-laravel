<?php
include "bd.php";

// print_r($_POST); die();

$name_user = $_POST['name_user'];
$email = $_POST['email'];
$address = $_POST['address'];

$sql = $conn->prepare("INSERT INTO tb_user(name_user, email, address) values (:name_user, :email, :address)");
$sql->bindParam(':name_user', $name_user);
$sql->bindParam(':email', $email);
$sql->bindParam(':address', $address);
if($sql->execute()){
    header("Location: " . '../index.php');
}
else{
    echo "Error register";
    echo $sql->errorInfo();
}
    