<?php

include 'bd.php';

$id = $_POST['id'];
$name_user = $_POST['name_user'];
$email = $_POST['email'];
$address = $_POST['address'];

$update = $conn->prepare("UPDATE tb_user SET name_user = :name_user, email = :email, address = :address where id = :id ");
$update->bindParam(":name_user", $name_user);
$update->bindParam(":email", $email);
$update->bindParam(":address", $address);
$update->bindParam(":id", $id);

if($update->execute()){
    header("Location: " . '../index.php');
}
else{
    echo "Error register";
    echo $sql->errorInfo();
}

