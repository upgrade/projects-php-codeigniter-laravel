<?php

$server = "localhost";
$user = "root";
$pass= "";

try{
    $conn = new PDO("mysql:host=$server; dbname=estudophppdo", $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // echo "Connected";

}
catch(PDOException $e){
    echo "Connection failed - ". $e->getMessage();
}