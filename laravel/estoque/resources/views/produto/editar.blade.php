@extends('layout.principal')
@section('conteudo')
<h1>Editar o produto {{$produto->nome}}</h1>
<form action="/produtos/salvarEdicao/{{$produto->id}}" method="post" >

<input type="hidden" name="_token" value="{{{ csrf_token() }}}">
<input type="hidden" class="form-control" name="id" value="{{$produto->id}}">

<div class="form-group">
    <label for="nome">Nome:</label>
    <input type="text" class="form-control" name="nome" value="{{$produto->nome}}">
</div>
<div class="form-group">
    <label for="descricao">Descrição:</label>
    <input type="text" name="descricao" class="form-control" value="{{$produto->descricao}}">
</div>

<div class="form-group">
    <label for="valor">Valor:</label>
    <input type="text" name="valor" class="form-control" value="{{$produto->valor}}">
</div>

<div class="form-group">
    <label for="quantidade">Quantidade:</label>
    <input type="number" name="quantidade" class="form-control" value="{{$produto->quantidade}}">
</div>

<button type="submit" class="btn btn-primary btn-block">Editar</button>

</form>

@stop