<?php namespace estoque\Http\Controllers;

use Illuminate\Support\Facades\DB;
use estoque\Produto;
use Request;
use estoque\Http\Requests\ProdutoRequest;

class ProdutoController extends Controller{

    public function __construct(){
        // $this->middleware('nosso-middleware', 
        // [
        //     'only' => ['adiciona', 'remover']
        // ]);

        $this->middleware('auth', 
        [
            'only' => ['adiciona', 'remover']
        ]);
    }

    public function lista(){
        $produtos = Produto::all();
        return view('produto.listagem')->with('produtos', $produtos);
    }

    public function mostra($id){
        //$id = Request::route('id');
        // $resposta = DB::select('select * from produtos where id = ?', [$id]);
        $resposta = Produto::find($id);
        if(empty($resposta)){
            return 'esse produto não existe';
        }
        return view('produto.detalhes')->with('p', $resposta);
    }

    public function novo(){
        return view('produto.formulario');
    }

    public function adiciona(ProdutoRequest $request){

        // $produto = new Produto();

        // $produto->nome = Request::input('nome');
        // $produto->descricao = Request::input('descricao');
        // $produto->valor = Request::input('valor');
        // $produto->quantidade = Request::input('quantidade');
        // $produto->save();

        // $params = Request::all();
        // $produto = new produto($params);
        // $produto->save();

        Produto::create($request->all());

        // DB::insert('insert into produtos
        // (nome, quantidade, valor, descricao) values (?,?,?,?)',
        // array($nome, $quantidade, $valor, $descricao));

       return redirect()
            ->action('ProdutoController@lista')
            ->withInput(Request::only('nome'));
    }

    public function listaJson(){
        $produtos = DB::select('select * from produtos');
        return $produtos;
    }

    public function editar($id){
        $produto = Produto::find($id);
        return view('produto.editar')->with('produto', $produto);
    }

    public function salvarEdicao($id){
      $produto = Produto::find($id);
      $produto->nome = Request::input('nome');
      $produto->descricao = Request::input('descricao');
      $produto->valor = Request::input('valor');
      $produto->quantidade = Request::input('quantidade');
      $produto->save();
      return redirect()->action('ProdutoController@lista');
    }

    public function remover($id){
        //print_r('sadasd');die();
        $produto = Produto::find($id);
        $produto->delete();
        return redirect()->action('ProdutoController@lista');
    }

}