<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institucional extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->output->cache(1440);
    }

    public function index(){
        $data['title'] = "LCI | Home";
        $data['description'] = "Teste CodeIgniter";
        $this->load->view('home', $data);
    }

    public function Empresa(){
        $data['title'] = "LCI | Empresa";
        $data['description'] = "Empresa CodeIgniter";
        $this->load->view('empresa', $data);
    }

    public function Servicos(){
        $data['title'] = "LCI | Serviços";
        $data['description'] = "Serviços CodeIgniter";
        $this->load->view('servicos', $data);
    }

}